package com.example.midtranspaymenttest.api

import com.example.midtranspaymenttest.data.request.PurchaseRequest
import com.example.midtranspaymenttest.data.response.PurchaseResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("charge")
    suspend fun sendPurchase(@Body purchaseRequest: PurchaseRequest): SuccessCallback<PurchaseResponse>
}

typealias SuccessCallback<T> = Response<T>