package com.example.midtranspaymenttest.ui

import android.os.Bundle
import android.provider.Telephony.BaseMmsColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.example.midtranspaymenttest.R
import com.example.midtranspaymenttest.data.request.CustomerDetails
import com.example.midtranspaymenttest.data.request.ItemDetails
import com.example.midtranspaymenttest.data.request.PurchaseRequest
import com.example.midtranspaymenttest.data.request.TransactionDetails
import com.example.midtranspaymenttest.databinding.FragmentProductBinding
import com.midtrans.sdk.corekit.core.MidtransSDK
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*
import android.provider.Telephony.BaseMmsColumns.TRANSACTION_ID

import com.midtrans.sdk.corekit.core.TransactionRequest
import com.midtrans.sdk.corekit.models.ShippingAddress
import com.midtrans.sdk.corekit.core.UIKitCustomSetting





/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ProductFragment : Fragment() {

    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ProductViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        observeData()
    }

    private fun observeData() {
        with(productViewModel) {
            observePurchaseResponse().onResult {
                // USE REDIRECT URL
            }
        }
    }

    private fun init() {
        with(binding) {
            btnPay.setOnClickListener {
                val prodId = etProductId.text.toString()
                val prodName = etProductName.text.toString()
                val prodPrice = etProductPrice.text.toString().toInt()
                val prodQty = etProductQuantity.text.toString().toInt()
                val totalPrice = prodPrice.times(prodQty)

                //USING MIDTRANS SDK
                val transactionRequest = TransactionRequest("$prodId-$prodName-$prodQty", totalPrice.toDouble())
                val customerDetails = com.midtrans.sdk.corekit.models.CustomerDetails(
                    "Peter",
                    "Parket",
                    "testerTransaksi@example.com",
                    "0812345678901"
                )
                // IF SHIPPING ADDRESS IS NOT PROVIDED, MIDTRANS WILL ASK DURING FOR IT PAYMENT
                val shippingAddress = ShippingAddress()
                shippingAddress.address = "test"
                shippingAddress.city = "test"
                shippingAddress.postalCode = "66666"
                customerDetails.shippingAddress = shippingAddress

                transactionRequest.itemDetails = arrayListOf(
                    com.midtrans.sdk.corekit.models.ItemDetails(
                        prodId,
                        prodPrice.toDouble(),
                        prodQty,
                        prodName
                    )
                )
//                transactionRequest.customerDetails = customerDetails

                // TO SKIP CUSTOMER DETAILS
                val uiSetting = MidtransSDK.getInstance().uiKitCustomSetting
                uiSetting.isSkipCustomerDetailsPages = true
                MidtransSDK.getInstance().uiKitCustomSetting = uiSetting

                // BASIC REQUEST
                MidtransSDK.getInstance().transactionRequest = transactionRequest
                MidtransSDK.getInstance().startPaymentUiFlow(context)

                // USING YOUR OWN
//                val itemDetail = ItemDetails(
//                    id = prodId,
//                    price = prodPrice,
//                    quantity = prodQty,
//                    name = prodName
//                )
//
//                val customerData = CustomerDetails(
//                    email = "testerTransaksi@example.com",
//                    firstName = "Peter",
//                    lastName = "Parket",
//                    phone = "0812345678901"
//                )
//
//                val purchaseData = PurchaseRequest(
//                    transactionDetails = TransactionDetails(
//                        orderId = "$prodId-$prodName-$prodQty",
//                        grossAmount = totalPrice
//                    ),
//                    itemDetails = listOf(itemDetail),
//                    customerDetails = listOf(customerData)
//                )
//                productViewModel.sendPurchase(purchaseData)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun <T> LiveData<T>.onResult(action: (T) -> Unit) {
        observe(this@ProductFragment, { data -> data?.let(action) })
    }
}