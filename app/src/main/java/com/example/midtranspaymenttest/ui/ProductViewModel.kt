package com.example.midtranspaymenttest.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.midtranspaymenttest.base.BaseViewModel
import com.example.midtranspaymenttest.data.AppRepository
import com.example.midtranspaymenttest.data.ResourceState
import com.example.midtranspaymenttest.data.request.PurchaseRequest
import com.example.midtranspaymenttest.data.response.PurchaseResponse
import kotlinx.coroutines.launch

class ProductViewModel(private val repository: AppRepository) : BaseViewModel() {
    private val purchaseResponse = MutableLiveData<PurchaseResponse>()

    fun observePurchaseResponse(): LiveData<PurchaseResponse> = purchaseResponse

    fun sendPurchase(purchaseRequest: PurchaseRequest) {
        viewModelScope.launch {
            when (val result = repository.sendPurchase(purchaseRequest)) {
                is ResourceState.Success -> {
                    result.result.data?.let {
                        purchaseResponse.postValue(it)
                    }
                }
                is ResourceState.Error -> {
                    errorResponse.postValue(result.error.errorResponse)
                }
            }
        }
    }
}