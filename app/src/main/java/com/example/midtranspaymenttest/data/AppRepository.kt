package com.example.midtranspaymenttest.data

import com.example.midtranspaymenttest.data.request.PurchaseRequest
import com.example.midtranspaymenttest.data.response.PurchaseResponse

class AppRepository(
    private val remoteDataSource: RemoteDataSource
) {
    suspend fun sendPurchase(purchaseRequest: PurchaseRequest): ResourceState<ResponseWrapper<PurchaseResponse>> {
        return remoteDataSource.sendPurchase(purchaseRequest)
    }

}