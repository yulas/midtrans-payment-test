package com.example.midtranspaymenttest.data

import com.example.midtranspaymenttest.api.ApiService
import com.example.midtranspaymenttest.data.request.PurchaseRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Response
import java.net.UnknownHostException
import javax.net.ssl.HttpsURLConnection

class RemoteDataSource(private val networkService: ApiService) {
    private suspend fun <T> getValue(request: suspend () -> Response<T>): ResourceState<ResponseWrapper<T>> {
        return try {
            val response = request()
            val body = response.body()

            if (response.isSuccessful.not() || body == null) {
                return errorState(response.code(), response.message())
            }

            if (response.code() !in (200..201)) {
                return errorState(response.code() , response.message())
            }

            return ResourceState.Success(
                ResponseWrapper(
                    response.message(),
                    response.body(),
                    null
                )
            )

        } catch (e: Exception) {
            errorState(msg = if (e is UnknownHostException) "NO_INTERNET" else e.localizedMessage)
        }
    }

    suspend fun sendPurchase(purchaseRequest: PurchaseRequest) = suspendDataResult {
        getValue {
            networkService.sendPurchase(purchaseRequest)
        }
    }

    private fun <T> errorState(
        errorCode: Int? = HttpsURLConnection.HTTP_INTERNAL_ERROR,
        msg: String
    ): ResourceState<ResponseWrapper<T>> {
        return ResourceState.Error(ResponseWrapper(msg, null, ErrorResponse(errorCode, msg)))
    }

    suspend fun <T> suspendDataResult(request: suspend () -> ResourceState<T>): ResourceState<T> {
        return withContext(Dispatchers.IO) {
            request.invoke()
        }
    }

    suspend fun suspendDataDownload(request: suspend () -> Response<ResponseBody>): Response<ResponseBody> {
        return withContext(Dispatchers.IO) {
            request.invoke()
        }
    }
}