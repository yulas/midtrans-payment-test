package com.example.midtranspaymenttest.data.request

import com.google.gson.annotations.SerializedName

data class PurchaseRequest(
    @SerializedName("transaction_details")
    val transactionDetails: TransactionDetails,
    @SerializedName("item_details")
    val itemDetails: List<ItemDetails>,
    @SerializedName("customer_details")
    val customerDetails: List<CustomerDetails>
)

data class TransactionDetails(
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("gross_amount")
    val grossAmount: Int
)

data class ItemDetails(
    @SerializedName("id")
    val id: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("quantity")
    val quantity: Int,
    @SerializedName("name")
    val name: String
)

data class CustomerDetails(
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("phone")
    val phone: String
)