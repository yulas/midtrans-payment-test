package com.example.midtranspaymenttest.data.response

import com.google.gson.annotations.SerializedName

data class PurchaseResponse(
    @SerializedName("token")
    val token: String,
    @SerializedName("redirect_url")
    val redirectUrl: String
)