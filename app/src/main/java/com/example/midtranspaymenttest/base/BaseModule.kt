package com.example.midtranspaymenttest.base

import org.koin.core.module.Module

interface BaseModule {
    val modules: List<Module>
}