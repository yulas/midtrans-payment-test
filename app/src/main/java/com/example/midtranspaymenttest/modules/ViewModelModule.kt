package com.example.midtranspaymenttest.modules

import com.example.midtranspaymenttest.base.BaseModule
import com.example.midtranspaymenttest.ui.ProductViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object ViewModelModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(viewModelModule)

    private val viewModelModule = module {
        viewModel { ProductViewModel(get()) }
    }
}