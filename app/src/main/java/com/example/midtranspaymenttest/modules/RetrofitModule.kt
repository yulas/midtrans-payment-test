package com.example.midtranspaymenttest.modules

import com.example.midtranspaymenttest.api.ApiService
import com.example.midtranspaymenttest.base.BaseModule
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(
            retrofitModule,
            webServiceModule
        )

    private val webServiceModule = module {
        single { get<Retrofit>().create(ApiService::class.java) }
    }

    private val retrofitModule = module {
//        single { providesHeaderInterceptor(get()) }
        single { provideOkHttpClient() }
        single { provideRetrofit(get()) }
    }

//    private fun providesHeaderInterceptor(sharedPreferenceManager: SharedPreferencesManager): HeaderInterceptor =
//        HeaderInterceptor(sharedPreferenceManager)

    private fun provideOkHttpClient(
//        headerInterceptor: HeaderInterceptor
    ): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
//            .addInterceptor(headerInterceptor)
            .addInterceptor(logging)
            .connectTimeout(30L, TimeUnit.SECONDS)
            .writeTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .build()
    }

    private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private const val BASE_URL = "https://testingwebylsm.000webhostapp.com/index.php/"
}