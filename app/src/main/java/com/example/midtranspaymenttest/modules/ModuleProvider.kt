package com.example.midtranspaymenttest.modules

import org.koin.core.module.Module

object ModuleProvider {
    val modules: List<Module>
        get() {
            return ArrayList<Module>().apply {
                addAll(RetrofitModule.modules)
                addAll(RepositoryModule.modules)
                addAll(ViewModelModule.modules)
            }
        }
}