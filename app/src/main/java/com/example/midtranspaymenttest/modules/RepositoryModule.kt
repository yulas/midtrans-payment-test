package com.example.midtranspaymenttest.modules

import com.example.midtranspaymenttest.base.BaseModule
import com.example.midtranspaymenttest.data.AppRepository
import com.example.midtranspaymenttest.data.RemoteDataSource
import org.koin.core.module.Module
import org.koin.dsl.module

object RepositoryModule : BaseModule {
    override val modules: List<Module>
        get() = listOf(
            dataRepoModules,
            remoteDataSourcesModule
        )

    private val dataRepoModules = module {
        single { AppRepository(get()) }
    }

    private val remoteDataSourcesModule = module {
        single { RemoteDataSource(get()) }
    }

//    private val localDataSourceModule = module {
//        single { LocalDataSource(get()) }
//    }
}